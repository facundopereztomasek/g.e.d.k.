#!/usr/bin/env bash
#G.E.D.K. for OSX
#Version 0.0.0


#TODO: Each software must be a separated script and this script should run all the scripts

clear
echo -e "\033[34mHi Five! Let's configure your environment!\033[0m"
echo
echo -e "\033[93m★\033[0m \033[33mInstalling Homebrew\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~~~~~~~\033[0m"

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" &> /dev/null

echo -e "\033[92m✓\033[0m \033[33mHomebrew installed!\033[0m"
echo -e "\033[93m★\033[0m \033[33mInstalling Homebrew Cask\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~~~~~~~~~~~~\033[0m"

brew tap caskroom/cask &> /dev/null
brew install caskroom/cask/brew-cask &> /dev/null
brew tap caskroom/versions &> /dev/null

echo -e "\033[92m✓\033[0m \033[33mHomebrew Cask installed!\033[0m"

echo -e "\033[93m★\033[0m \033[33mInstalling Git\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~033[0m"

brew install git &> /dev/null

echo -e "\033[92m✓\033[0m \033[33mGit installed!\033[0m"

echo -e "\033[93m★\033[0m \033[33mInstalling NodeJS\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~~~\033[0m"

brew install node &> /dev/null

echo -e "\033[92m✓\033[0m \033[33mNodeJS installed!\033[0m"

echo -e "\033[93m★\033[0m \033[33mInstalling Google Chrome\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~~~~~~~~~~\033[0m"

brew cask install google-chrome &> /dev/null

echo -e "\033[92m✓\033[0m \033[33mGoogle Chrome installed!\033[0m"


echo -e "\033[93m★\033[0m \033[33mInstalling Sublime Text 3\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\033[0m"

brew cask install caskroom/versions/sublime-text3 &> /dev/null

echo -e "\033[92m✓\033[0m \033[33mSublime Text 3 installed!\033[0m"

ln -s "$(which subl)" /usr/local/bin/sublime &> /dev/null

echo -e "\033[96m↪\033[0m \033[37mNow you can run: \033[97m$ sublime file\033[0m"

echo -e "\033[93m★\033[0m \033[33mInstalling iTerm2\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~~~~\033[0m"

brew cask install caskroom/homebrew-cask/iterm2 &> /dev/null
echo -e "\033[92m✓\033[0m \033[33mIterm2 installed!\033[0m"

echo -e "\033[93m★\033[0m \033[33mInstalling oh-my-zsh\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~~~~~~~\033[0m"

git clone git://github.com/bwithem/oh-my-zsh.git ~/.oh-my-zsh &> /dev/null
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc &> /dev/null
chsh -s /bin/zsh &> /dev/null
sed -i -e 's/robbyrussell/agnoster/g' ~/.zshrc &> /dev/null

echo -e "\033[96m↪\033[0m \033[37mPlease install Dracula Theme for iTerm2\033[0m"
open https://zenorocha.github.io/dracula-theme/iterm/ &> /dev/null

echo -e "\033[93m★\033[0m \033[33mInstalling menlo for powerline font\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~~~~~~~\033[0m"

git clone git://github.com/abertsch/Menlo-for-Powerline.git ~/menlo-fonts/ &> /dev/null
open ~/menlo-fonts/*.ttf &> /dev/null

echo -e "\033[96m↪\033[0m \033[37mPlease install Menlo Powerline font for iTerm2\033[0m"

echo -e "\033[93m★\033[0m \033[33mInstalling SourceTree\033[0m"
echo -e "\033[33m~~~~~~~~~~~~~~~~~~\033[0m"

brew cask install caskroom/homebrew-cask/sourcetree &> /dev/null
echo -e "\033[92m✓\033[0m \033[33mSourceTree installed!\033[0m"
#
#git clone https://github.com/zenorocha/dracula-theme/ ~/dracula-theme
#
#cp ~/dracula-theme/zsh/dracula.zsh-theme ~/.oh-my-zsh/themes/dracula.zsh-theme
#rm -rf ~/dracula-theme
#echo -e "\033[92m✓\033[0m \033[33moh-my-zsh installed!\033[0m"
                                              
echo -e "\033[91m"                                                              
echo -e '                                                    .;          '
echo -e '                                                 `;;;;;         '
echo -e '                                               :;;;;;;;;        '
echo -e '                                            :;;;;;;;;;;;;       '
echo -e '                                         ,;;;;;;;;;;;;;;;;`     '
echo -e '                                      `;;;;;;;;;;;;;;;;;;;;`    '
echo -e '                                      ;;;;;;;;;;;;;;;;;;;;;;.   '
echo -e '                                      ,;;;;;;;;;;;;;;;;;;;;;;.  '
echo -e '                                       ;;;;;;;;;;;;;;;;;;;;;;;. '
echo -e '                                       ;;;;;;;;;;;;;;;;;;;;;;;;.'
echo -e '        \033[33mmade with \033[91m♥\033[33m by aerolab\033[91m         .;;;;;;;;;;;;;;;;;;;;;;; '
echo -e '                                        ;;;;;;;;;;;;;;;;;;;;;,  '
echo -e '                                        ;;;;;;;;;;;;;;;;;;;;    '
echo -e '                                        `;;;;;;;;;;;;;;;;;:     '
echo -e '                                         ;;;;;;;;;;;;;;;;`      '
echo -e '                                         :;;;;;;;;;;;;;;        '
echo -e '                                          ;;;;;;;;;;;;`         '
echo -e '                                          ;;;;;;;;;;;           '
echo -e '            ``....``                      ,;;;;;;;;,            '
echo -e '       ,;;;;;,.````.,;;,                   ;;;;;;;              '
echo -e '    :;;;,               ;;                 ;;;;;:               '
echo -e '  ;;;,                    ;.               .;;;`                '
echo -e '  ;`                       .;               ;:                  '
echo -e '                             ;`            ;                    '
echo -e '                              `;.       .;`                     '
echo -e '                                 .:;;;:.                        '
echo -e "\033[0m"