#G.E.D.K. v0.0.0 (OS X)
##Garden of Eden Developer Kit

Author: Facundo P�rez | [facundopereztomasek@gmail.com](mailto://facundopereztomasek@gmail.com) | [https://bitbucket.org/facundopereztomasek](https://bitbucket.org/facundopereztomasek)

This version of G.E.D.K. run on top of Homebrew and therefore it only will run on OS X.

G.E.D.K. will install the next software:

+ Homebrew
+ Homebrew Cask
+ Git
+ NodeJS
+ Google Chrome
+ Sublime Text 3
+ iTerm2
+ oh-my-zsh
+ menlo for powerline font
+ SourceTree

